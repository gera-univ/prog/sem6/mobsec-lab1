Depends on [cJSON](https://github.com/DaveGamble/cJSON).

```
$ gcc main.c -I/usr/include/cjson/ -lcjson

$ ./a.out -j /opt/openjdk-bin-11.0.14_p9/bin/
[{
		"path":	"/opt/openjdk-bin-11.0.14_p9/bin/jcmd",
		"uid":	"0",
		"gid":	"0",
		"mode":	"rwxr-xr-x",
		"atime":	"Thu Feb  3 16:38:40 2022\n",
		"mtime":	"Thu Feb  3 16:38:44 2022\n",
		"ctime":	"Thu Feb  3 16:38:47 2022\n",
		"size":	"10064"
	}, {
...
	}, {
		"path":	"/opt/openjdk-bin-11.0.14_p9/bin/keytool",
		"uid":	"0",
		"gid":	"0",
		"mode":	"rwxr-xr-x",
		"atime":	"Thu Feb  3 16:38:40 2022\n",
		"mtime":	"Thu Feb  3 16:38:44 2022\n",
		"ctime":	"Thu Feb  3 16:38:47 2022\n",
		"size":	"10064"
	}]

$ ./a.out /opt/openjdk-bin-11.0.14_p9/bin/
file: /opt/openjdk-bin-11.0.14_p9/bin/jcmd
uid:                      0
gid:                      0
mode:                     rwxr-xr-x
atime:                    Thu Feb  3 16:38:40 2022
mtime:                    Thu Feb  3 16:38:44 2022
ctime:                    Thu Feb  3 16:38:47 2022
size:                     10064 bytes
...
file: /opt/openjdk-bin-11.0.14_p9/bin/keytool
uid:                      0
gid:                      0
mode:                     rwxr-xr-x
atime:                    Thu Feb  3 16:38:40 2022
mtime:                    Thu Feb  3 16:38:44 2022
ctime:                    Thu Feb  3 16:38:47 2022
size:                     10064 bytes
```
