#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <cJSON.h>
#include <limits.h>

#define STR_SIZE 1024
#define FP_SPECIAL 1

cJSON *json_root = NULL;

// from Kerrisk
char *filePermStr(mode_t perm, int flags)
{
    static char str[STR_SIZE];

    snprintf(str, STR_SIZE, "%c%c%c%c%c%c%c%c%c",
        (perm & S_IRUSR) ? 'r' : '-', (perm & S_IWUSR) ? 'w' : '-',
        (perm & S_IXUSR) ?
            (((perm & S_ISUID) && (flags & FP_SPECIAL)) ? 's' : 'x') :
            (((perm & S_ISUID) && (flags & FP_SPECIAL)) ? 'S' : '-'),
        (perm & S_IRGRP) ? 'r' : '-', (perm & S_IWGRP) ? 'w' : '-',
        (perm & S_IXGRP) ?
            (((perm & S_ISGID) && (flags & FP_SPECIAL)) ? 's' : 'x') :
            (((perm & S_ISGID) && (flags & FP_SPECIAL)) ? 'S' : '-'),
        (perm & S_IROTH) ? 'r' : '-', (perm & S_IWOTH) ? 'w' : '-',
        (perm & S_IXOTH) ?
            (((perm & S_ISVTX) && (flags & FP_SPECIAL)) ? 't' : 'x') :
            (((perm & S_ISVTX) && (flags & FP_SPECIAL)) ? 'T' : '-'));

    return str;
}

void print_file_info(struct stat sb, const char *path)
{
  printf("file: %s\n", path);
  printf("uid:                      %d\n", sb.st_uid);
  printf("gid:                      %d\n", sb.st_gid);
  printf("mode:                     %s\n", filePermStr(sb.st_mode, FP_SPECIAL));
  printf("atime:                    %s", ctime(&sb.st_atime));
  printf("mtime:                    %s", ctime(&sb.st_mtime));
  printf("ctime:                    %s", ctime(&sb.st_ctime));
  printf("size:                     %lld bytes\n", (long long) sb.st_size);
}

void add_file_info_to_json(struct stat sb, const char *path)
{
  char str[STR_SIZE];
  cJSON *entry = cJSON_CreateObject();
  cJSON_AddItemToObject(entry, "path", cJSON_CreateString(path));
  snprintf(str, STR_SIZE, "%d", sb.st_uid);
  cJSON_AddItemToObject(entry, "uid", cJSON_CreateString(str));
  snprintf(str, STR_SIZE, "%d", sb.st_gid);
  cJSON_AddItemToObject(entry, "gid", cJSON_CreateString(str));
  snprintf(str, STR_SIZE, "%s", filePermStr(sb.st_mode, FP_SPECIAL));
  cJSON_AddItemToObject(entry, "mode", cJSON_CreateString(str));
  snprintf(str, STR_SIZE, "%s", ctime(&sb.st_atime));
  cJSON_AddItemToObject(entry, "atime", cJSON_CreateString(str));
  snprintf(str, STR_SIZE, "%s", ctime(&sb.st_mtime));
  cJSON_AddItemToObject(entry, "mtime", cJSON_CreateString(str));
  snprintf(str, STR_SIZE, "%s", ctime(&sb.st_ctime));
  cJSON_AddItemToObject(entry, "ctime", cJSON_CreateString(str));
  snprintf(str, STR_SIZE, "%lld", (long long) sb.st_size);
  cJSON_AddItemToObject(entry, "size", cJSON_CreateString(str));
  cJSON_AddItemToArray(json_root, entry);
}

void walk_dirs(char path[])
{
  char cwd[PATH_MAX];
  if (chdir(path) != 0) {
    getcwd(cwd, sizeof(cwd));
    char error[STR_SIZE];
    snprintf(error, STR_SIZE, "cd %s/%s", cwd, path);
    perror(error);
    return;
  }
  getcwd(cwd, sizeof(cwd));
  DIR *dirp = opendir(".");
  if (dirp == NULL) {
    perror("opendir");
    return;
  }

  int currfd = open(".", O_RDONLY);
  struct stat sb;
  struct dirent *dent;
  for (;;) {
    errno = 0;
    dent = readdir(dirp);
    if (dent == NULL)
      break;
    if (strcmp(dent->d_name, ".") == 0 || strcmp(dent->d_name, "..") == 0)
      continue;
    if (lstat(dent->d_name, &sb) == -1) {
      perror("lstat");
      continue;
    }

    switch (sb.st_mode & S_IFMT) {
      case S_IFREG:
        char full_path[PATH_MAX];
        snprintf(full_path, STR_SIZE, "%s/%s", cwd, dent->d_name);

        if (json_root) {
          add_file_info_to_json(sb, full_path);
        } else {
          print_file_info(sb, full_path);
        }

        break;
      case S_IFDIR:
        walk_dirs(dent->d_name);
        fchdir(currfd);
        break;
      default:
        break;
    }
  }
  closedir(dirp);
}

void eusage(char *argv0)
{
  fprintf(stderr, "usage: %s [-j] directory\n", argv0);
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
  int opt;
  while ((opt = getopt(argc, argv, "j")) != -1) {
    switch (opt) {
    case 'j':
      json_root = cJSON_CreateArray();
      break;
    default: eusage(argv[0]);
    }
  }

  if (argc - optind > 1) eusage(argv[0]);
  if (optind == argc)
    walk_dirs(".");
  else
    walk_dirs(argv[optind]);

  if (json_root) {
    char *out = cJSON_Print(json_root);
    printf("%s\n", out);
  }

  return 0;
}
